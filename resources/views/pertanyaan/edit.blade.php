@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan {{$pertanyaanId->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/pertanyaan/{{$pertanyaanId->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                
                  <div class="form-group">
                    <label for="judul_pertanyaan">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="judul_pertanyaan" name="judul_pertanyaan" value="{{old('judul_pertanyaan', $pertanyaanId->judul_pertanyaan)}}" placeholder="Masukkan Judul Pertanyaan" required>
                    @error('judul_pertanyaan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi_pertanyaan">Isi Pertanyaan</label>
                    <input type="text" class="form-control" id="isi_pertanyaan" name="isi_pertanyaan" value="{{old('isi_pertanyaan', $pertanyaanId->isi_pertanyaan)}}" placeholder="Masukkan Pertanyaan" required>
                    @error('isi_pertanyaan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
</div>

@endsection