@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Buat Pertanyaan Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/pertanyaan" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul_pertanyaan">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="judul_pertanyaan" name="judul_pertanyaan" value="{{old('judul_pertanyaan', '')}}" placeholder="Masukkan Judul Pertanyaan" required>
                    @error('judul_pertanyaan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi_pertanyaan">Isi Pertanyaan</label>
                    <input type="text" class="form-control" id="isi_pertanyaan" name="isi_pertanyaan" value="{{old('isi_pertanyaan', '')}}" placeholder="Masukkan Pertanyaan" required>
                    @error('isi_pertanyaan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Buat</button>
                </div>
              </form>
            </div>
</div>

@endsection