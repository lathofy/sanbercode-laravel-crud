@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tampilan Data Pertanyaan</h3>
            </div>

            <div class="card-body">
                <h4>{{$pertanyaanId->judul_pertanyaan}}</h4>
                <p>{{$pertanyaanId->isi_pertanyaan}}</p>
            </div>

            <div class="card-footer">
                <a href="/pertanyaan" class="btn btn-primary mb-2">Kembali</a>
            </div>
        </div>
    </div>
@endsection