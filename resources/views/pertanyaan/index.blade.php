@extends('adminlte.master')

@section('content')
    <div class="ml-3 mt-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('Sukses'))
              <div class="alert alert-success">
              {{session('Sukses')}}
              </div>
              @endif
              <a href="{{route('pertanyaan.create')}}" class="btn btn-primary mb-2">Buat Pertanyaan Baru</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul Pertanyaan</th>
                      <th>Isi Pertanyaan</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pertanyaan as $key => $item)
                    <tr>
                        <td> {{$key + 1}}</td>
                        <td> {{$item->judul_pertanyaan}}</td>
                        <td> {{$item->isi_pertanyaan}}</td>
                        <td style="display: flex;">
                            <a href="{{route('pertanyaan.show', ['pertanyaan' => $item->id])}}" class="btn btn-info btn-sn mr-1">Tampil</a>
                            <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-default btn-sn mr-1">Ubah</a>
                            <form action="/pertanyaan/{{$item->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Hapus" class="btn btn-danger">
                            </form>
                        </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center">Tidak Ada Data Pertanyaan</td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- 
                  /.card-body 
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
              -->
            </div>
    </div>
@endsection