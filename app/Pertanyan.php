<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyan extends Model
{
    protected $table = "pertanyaan";
    protected $fillable = ["judul_pertanyaan", "isi_pertanyaan"];
}
