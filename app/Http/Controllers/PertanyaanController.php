<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyan;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'judul_pertanyaan' => 'required|unique:pertanyaan',
            'isi_pertanyaan' => 'required'
        ]);

        /*
        $query = DB::table('pertanyaan')->insert([
            "judul_pertanyaan" => $request["judul_pertanyaan"],
            "isi_pertanyaan" => $request["isi_pertanyaan"]
            ]);
            */

            /*
            $pertanyaans = new Pertanyan;
            $pertanyaans->judul_pertanyaan = $request->judul_pertanyaan;
            $pertanyaans->isi_pertanyaan = $request->isi_pertanyaan;
            $pertanyaans->save();
            */
            
            $pertanyaans = Pertanyan::create([
                "judul_pertanyaan" => $request["judul_pertanyaan"],
                "isi_pertanyaan" => $request["isi_pertanyaan"]
            ]);

            return redirect('/pertanyaan')->with('Sukses', 'Pertanyaan Berhasil Disimpan!!');
    }

    public function index(){
        //$pertanyaan = DB::table('pertanyaan')->get();
        //dd($pertanyaan->all());

        $pertanyaan = Pertanyan::all();

        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($pertanyaan_id){
        //$pertanyaanId = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        //dd($pertanyaanId);
        $pertanyaanId = Pertanyan::find($pertanyaan_id);

        return view('pertanyaan.show', compact('pertanyaanId'));
    }

    public function edit($pertanyaan_id){
        //$pertanyaanId = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        $pertanyaanId = Pertanyan::find($pertanyaan_id);

        return view('pertanyaan.edit', compact('pertanyaanId'));
    }

    public function update($pertanyaan_id, Request $request){
        $request->validate([
            'judul_pertanyaan' => 'required',
            'isi_pertanyaan' => 'required'
        ]);
        /*
        $query = DB::table('pertanyaan')
        ->where('id', $pertanyaan_id)
        ->update([
            'judul_pertanyaan' => $request['judul_pertanyaan'],
            'isi_pertanyaan' => $request['isi_pertanyaan']
        ]);
        */
        $query = Pertanyan::where('id', $pertanyaan_id)->update([
            "judul_pertanyaan" =>$request["judul_pertanyaan"],
            "isi_pertanyaan" =>$request["isi_pertanyaan"]
        ]);

        return redirect('/pertanyaan')->with('Sukses', 'Pertanyaan Berhasil Di-update!!');
    }

    public function destroy($pertanyaan_id){
        //$query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        Pertanyan::destroy($pertanyaan_id);
        return redirect('/pertanyaan')->with('Sukses', 'Pertanyaan Berhasil Dihapus!!');
    }
}
